// defines pins numbers
// motor esquerda
int spdleftmotor=3;
int dirleftmotor=12;
// motor direita
int spdrightmotor=11;
int dirrightmotor=13;
// sensores da frente
const int trigPinf = 4;
const int echoPinf = 2;
// sensores da direita 
const int trigPinr = 5;
const int echoPinr = 6;
// sensores da esquerda 
const int trigPinl = 10;
const int echoPinl = 7;

// defines variables
 int state;
 int s;
 int q;
 long durationf;
 int distancef;
 long durationr;
 int distancer;
 long durationl;
 int distancel;
 int count1=0;
 int count2=0;
 int lerA4;
 int lerA5;
 int tmp;
 
void setup() {
pinMode(trigPinf, OUTPUT);
pinMode(echoPinf, INPUT);
pinMode(trigPinr, OUTPUT);
pinMode(echoPinr, INPUT);
pinMode(trigPinl, OUTPUT);
pinMode(echoPinl, INPUT);
pinMode(12,OUTPUT);
pinMode(3,OUTPUT);
pinMode(11,OUTPUT);
pinMode(13,OUTPUT);
pinMode(A5,INPUT);
pinMode(A4,INPUT);
Serial.begin(9600);

}

void idle()
{
  
  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,0);
  analogWrite(spdrightmotor,0); 
   
}

void forward(int spd)
{
  
  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,spd); 
   
}

void backward(int spd)
{

  digitalWrite(dirleftmotor,LOW);
  digitalWrite(dirrightmotor,LOW); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,spd); 
  
}

void left(int spd)
{

  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,0); 
  
}

void right(int spd)
{

  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,0);
  analogWrite(spdrightmotor,spd); 
  
}

void ABS()
{

  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,0);
  analogWrite(spdrightmotor,0);

}
void rotateleft(int spd)
{

  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,LOW); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,spd); 
  
}

void rotateright(int spd)
{

  digitalWrite(dirleftmotor,LOW);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,spd); 
  
}

void rotateright90(int spd)
{
  tmp=1420;
  digitalWrite(dirleftmotor,LOW);
  digitalWrite(dirrightmotor,HIGH); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,spd);
  delay(tmp); 

}

void rotateleft90(int spd)
{
  tmp=1420;
  digitalWrite(dirleftmotor,HIGH);
  digitalWrite(dirrightmotor,LOW); 
  analogWrite(spdleftmotor,spd);
  analogWrite(spdrightmotor,spd);
  delay(tmp);  
  
}

// codigo para ler o sensor da frente
void readf()
{

 // Clears the trigPin
digitalWrite(trigPinf, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPinf, HIGH);
delayMicroseconds(10);
digitalWrite(trigPinf, LOW);
durationf = pulseIn(echoPinf, HIGH);

// Calculating the distance
distancef= durationf*0.034/2;
Serial.println("frente");
Serial.println(distancef);

}

// codigo para ler o sensor da direita
void readr()
{

   // Clears the trigPin
digitalWrite(trigPinr, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPinr, HIGH);
delayMicroseconds(10);
digitalWrite(trigPinr, LOW);
durationr = pulseIn(echoPinr, HIGH);

// Calculating the distance
distancer= durationr*0.034/2;
Serial.println("direita");
Serial.println(distancer);

}

// codigo para ler o sensor da esquerda
void readl()
{

   // Clears the trigPin
digitalWrite(trigPinl, LOW);
delayMicroseconds(2);

// Sets the trigPin on HIGH state for 10 micro seconds
digitalWrite(trigPinl, HIGH);
delayMicroseconds(10);
digitalWrite(trigPinl, LOW);
durationl = pulseIn(echoPinl, HIGH);


// Calculating the distance
distancel= durationl*0.034/2;
Serial.println("esquerda");
Serial.println(distancel);
}

void readbotao(){
lerA4=digitalRead(A4);
lerA5=digitalRead(A5);  
}
/*
void roboautonomo(){
    readf();
    readr();
    readl();
if (distancef > 20 ) { //if an object is more than 20 cm away 
  s=0;
}
if (distancer < 10) { //if an object is less than 10 cm away
  s=1;
}
if (distancel< 10) { //if an object is less than 10 cm away
  s=2; 
  
}
switch (s){
  case 0:
  forward(200);
  break;
  
  case 1:
  while (distancer < 10 && lerA5 == HIGH){ 
  readr();  
  rotateleft(200);delay(50);
  readbotao();
  }  
  break;
  
  case 2:
  while (distancel < 10 && lerA5 == HIGH){
  readl();
  rotateright(200);delay(50);
  readbotao();
  } 
  break;
}
}

void quarto(){
  count1=0;
while(count1!=5 && lerA5 == HIGH) { //if an object is less than 20 cm away 
  readbotao();     
  readf();
  readr();
  readl();
  roboautonomo();
if(distancef < 20 ) { //if an object is less than 20 cm away 
  q=0; 
}  
if(distancef < 20 ) { //if an object is less than 20 cm away 
  q=1; 
}
if(distancel>30 ){
  q=2;
}
switch (q){
  case 0:  
  forward(200);
  break;
  
  case 1:  
  rotateright90(200);
  count1++;
  break;
  
  case 2:
  rotateleft90(200);
  count1++;
  break;
}
}
}
*/
void loop() { 
readbotao();
if (lerA4 == LOW){
readbotao();

while (lerA5 == HIGH){
readbotao(); 
    
readf();
readr();
readl();  
// codigo autonomo
if (distancef > 20 ) { //if an object is more than 20 cm away 
  state=0;
}
if (distancef < 20 && distancer < distancel ) { //if an object is less than 20 cm away 
  state=1; 
}
if (distancef < 20 && distancel < distancer ) { //if an object is less than 20 cm away
  state=2;   
}
if (distancer < 10) { //if an object is less than 10 cm away
  state=3;
}
if (distancel< 10) { //if an object is less than 10 cm away
  state=4; 
}
// predefinido 
  // cruzamentos 
if ( distancer > 60 ) { 
  state=6;
}
/*
//P3
if (distancer>115 && distancer<140){
  state=7;
}

//P9
if (  distancel > 70 && distancel < 85 && distancer > 10 && distancer < 20) {   
  state=8;
}
//P10
if (  distancel > 10 && distancel < 20 && distancer > 70 && distancer < 85 ) {  
  state=9;
}
*/

switch (state){
  case 0:
  forward(200);
  break;
  
  case 1:
 // while (distancef < 20 && lerA5 == HIGH){  
 // readf();  
  rotateleft90(200);
 //  readbotao();
 // } 
  break;
  
  case 2:
 // while (distancef < 20 && lerA5 == HIGH){  
 // readf();  
  rotateright90(200);
 //  readbotao();
 // } 
  break;
  
  case 3:
  while (distancer < 10 && lerA5 == HIGH){ 
  readr();  
  rotateleft(200);delay(50);
  readbotao();
  }  
  break;
  
  case 4:
  while (distancel < 10 && lerA5 == HIGH){
  readl();
  rotateright(200);delay(50);
  readbotao();
  } 
  break;
  
  case 5:
  rotateright(200);
  delay(2900);
  break;
  
 case 6:
  forward(200);
  delay(500);
  rotateright90(200);
  forward(200);
  delay(1000);

  //quarto();
  break;
  
}
}
}
}

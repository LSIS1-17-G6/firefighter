/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import java.sql.Connection;
import controller.ElementoController;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class ElementoUI {
    private final ElementoController controller;
    
    public ElementoUI(Connection connector){
        controller = new ElementoController(connector);
    }
    
    public void postElemento() throws Exception{
        
        int IDElemento=Utils.IntFromConsole("Introduza o número identificador do elemento: ");
        String nomeElemento= Utils.readLineFromConsole("Introduza o nome do elemento: ");
        int idade= Utils.IntFromConsole("Introduza a idade do elemento: ");
        
        controller.post(IDElemento, nomeElemento, idade);
    }
    
    public void deleteElemento() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do elemento a eliminar: ");
        
        controller.delete(id);
    }
    
    public void editElemento() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID do elemento que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDElemento=Utils.IntFromConsole("Número identificador do elemento: ");
        String nomeElemento= Utils.readLineFromConsole("Nome do elemento: ");
        int idade= Utils.IntFromConsole("Idade do elemento: ");
        
        controller.update(idProcura, IDElemento, nomeElemento, idade);
        
    }

}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.RoboController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class RoboUI {
    private final RoboController controller;
    
    public RoboUI(Connection connector){
        controller = new RoboController(connector);
    }
    
    public void postRobo() throws Exception{
        
        int IDRobo=Utils.IntFromConsole("Introduza o número identificador do robo: ");
        String nomeRobo= Utils.readLineFromConsole("Introduza o nome do robo: ");
        
        controller.post(IDRobo, nomeRobo);
    }
    
    public void deleteRobo() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do robo a eliminar: ");
        
        controller.delete(id);
    }
    
    public void editRobo() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID do robo que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDRobo=Utils.IntFromConsole("Número identificador do robo: ");
        String nomeRobo= Utils.readLineFromConsole("Nome do robo: ");
        
        controller.update(idProcura, IDRobo, nomeRobo);
        
    }
    
    public void getRobo() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do robo que pretende saber o nome");
        
        controller.get(id);
    }
    
}

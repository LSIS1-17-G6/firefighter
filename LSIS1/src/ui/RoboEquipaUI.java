/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.RoboEquipaController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class RoboEquipaUI {
    private final RoboEquipaController controller;
    
    public RoboEquipaUI(Connection connector){
        controller = new RoboEquipaController(connector);
    }
    
    public void postRoboEquipa() throws Exception{
        
        int IDRobo=Utils.IntFromConsole("Introduza o ID do robo a atribuir uma equipa: ");
        int IDEquipa=Utils.IntFromConsole("Introduza o ID da equipa associada ao robo");
        
        controller.post(IDRobo, IDEquipa);
    }
    
    public void deleteRoboEquipa() throws Exception{
        
        int IDRobo=Utils.IntFromConsole("Introduza o ID do robo a eliminar da equipa: ");
        
        controller.delete(IDRobo);
    }
    
    public void getRoboEquipa() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do robo que pretende saber a equipa");
        
        controller.get(id);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.ParametroRoboController;
import controller.PertenceController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class ParametroRoboUI {
    private final ParametroRoboController controller;
    
    public ParametroRoboUI(Connection connector){
        controller = new ParametroRoboController(connector);
    }
    
    public void postParametro() throws Exception{
        
        int IDRobo=Utils.IntFromConsole("Introduza o número identificador do robo: ");
        int IDMaterial= Utils.IntFromConsole("Introduza o número identificador do material: ");
        
        controller.post(IDRobo, IDMaterial);
    }
    
    public void deleteParametro() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do material a eliminar do robo");
        
        controller.delete(id);
    }
    
    public void getParametro() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do robo que pretende mostrar os materiais");
        
        controller.get(id);
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import utils.MENUME;
import utils.Utils;

public class MenuUI {
    
    private static Connection conn;
    //private Elemento m_elemento;
    private String opcao;
    private String escolha;
    
    
    public MenuUI(Connection conetar)
    {
        
        conn=conetar;
    }
    
    public void run() throws IOException, FileNotFoundException, ClassNotFoundException, Exception
    {
        do
        {
            
            
            System.out.println("\n\n");
            System.out.println("1. Elemento");
            System.out.println("2. Equipa");
            System.out.println("3. Associar Elementos e Equipas");
            System.out.println("4. Robo");
            System.out.println("5. Atribuir robo a uma Equipa");
            System.out.println("6. Materiais");
            System.out.println("7. Introduzir Materiais ao Robo");
            System.out.println("8. Prova");
            System.out.println("9. Mostrar Informacoes");
            System.out.println("10. Animacao");
            System.out.println("0. Sair");

            opcao = Utils.readLineFromConsole("Introduza opção: ");

            if( opcao.equals("1") )
            {
                do{ 
                    
                System.out.println("\n");
                System.out.println("1. Introduzir elemento");
                System.out.println("2. Modificar elemento");
                System.out.println("3. Eliminar elemento");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    ElementoUI ui = new ElementoUI(conn);
                    ui.postElemento();
                }
                
                if (escolha.equals("2")){
                    ElementoUI ui = new ElementoUI(conn);
                    ui.editElemento();
                }
                
                if (escolha.equals("3")){
                    ElementoUI ui = new ElementoUI(conn);
                    ui.deleteElemento();
                }
                
                
                }while (!escolha.equals("0") );
                
            }
            
            if( opcao.equals("2") )
            {
                do {
                System.out.println("\n");
                System.out.println("1. Introduzir equipa");
                System.out.println("2. Modificar equipa");
                System.out.println("3. Eliminar equipa");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    EquipaUI ui = new EquipaUI(conn);
                    ui.postEquipa();
                }
                
                if (escolha.equals("2")){
                    EquipaUI ui = new EquipaUI(conn);
                    ui.editEquipa();
                }
                
                if (escolha.equals("3")){
                    EquipaUI ui = new EquipaUI(conn);
                    ui.deleteEquipa();
                }
                
                }while (!escolha.equals("0") );
                
                }
            if( opcao.equals("3") )
            {
                do{ 
                    
                System.out.println("\n");
                System.out.println("1. Introduzir associacao");
                System.out.println("2. Eliminar associacao");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    PertenceUI ui = new PertenceUI(conn);
                    ui.postPertence();
                }
                
                if (escolha.equals("2")){
                    PertenceUI ui = new PertenceUI(conn);
                    ui.deletePertence();
                }
                
                }while (!escolha.equals("0") );
                
            }
            
             if( opcao.equals("4") )
            {
                do {
                System.out.println("\n");
                System.out.println("1. Introduzir robo");
                System.out.println("2. Modificar robo");
                System.out.println("3. Eliminar robo");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    RoboUI ui = new RoboUI(conn);
                    ui.postRobo();
                }
                
                if (escolha.equals("2")){
                    RoboUI ui = new RoboUI(conn);
                    ui.editRobo();
                }
                
                if (escolha.equals("3")){
                    RoboUI ui = new RoboUI(conn);
                    ui.deleteRobo();
                }
                
                }while (!escolha.equals("0") );
                
                }
             
             if( opcao.equals("5") )
            {
                do{ 
                    
                System.out.println("\n");
                System.out.println("1. Introduzir associacao");
                System.out.println("2. Eliminar associacao");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    RoboEquipaUI ui = new RoboEquipaUI(conn);
                    ui.postRoboEquipa();
                }
                
                if (escolha.equals("2")){
                    RoboEquipaUI ui = new RoboEquipaUI(conn);
                    ui.deleteRoboEquipa();
                }
                
                
                }while (!escolha.equals("0") );
                
            }
             
             if( opcao.equals("6") )
            {
                do {
                System.out.println("\n");
                System.out.println("1. Introduzir material");
                System.out.println("2. Modificar material");
                System.out.println("3. Eliminar material");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    MateriaisUI ui = new MateriaisUI(conn);
                    ui.postMateriais();
                }
                
                if (escolha.equals("2")){
                    MateriaisUI ui = new MateriaisUI(conn);
                    ui.editMateriais();
                }
                
                if (escolha.equals("3")){
                    MateriaisUI ui = new MateriaisUI(conn);
                    ui.deleteMateriais();
                }
                
                }while (!escolha.equals("0") );
                
                }
             
             if( opcao.equals("7") )
            {
                do{ 
                    
                System.out.println("\n");
                System.out.println("1. Introduzir Componente");
                System.out.println("2. Eliminar Componente");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    ParametroRoboUI ui = new ParametroRoboUI(conn);
                    ui.postParametro();
                }
                
                if (escolha.equals("2")){
                    ParametroRoboUI ui = new ParametroRoboUI(conn);
                    ui.deleteParametro();
                }
                
                
                }while (!escolha.equals("0") );
                
            }
             
             if( opcao.equals("8") )
            {
                do {
                System.out.println("\n");
                System.out.println("1. Introduzir prova");
                System.out.println("2. Modificar prova");
                System.out.println("3. Eliminar prova");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    ProvaRoboUI ui = new ProvaRoboUI(conn);
                    ui.postProvaRobo();
                }
                
                if (escolha.equals("2")){
                    ProvaRoboUI ui = new ProvaRoboUI(conn);
                    ui.editProvaRobo();
                }
                
                if (escolha.equals("3")){
                    ProvaRoboUI ui = new ProvaRoboUI(conn);
                    ui.deleteProvaRobo();
                }
                
                }while (!escolha.equals("0") );
                
                }
             
            if( opcao.equals("9") )
            {
                do {
                System.out.println("\n");
                System.out.println("1. Mostrar o nome da equipa");
                System.out.println("2. Mostrar elementos duma equipa");
                System.out.println("3. Mostrar materiais constituintes do robo");
                System.out.println("4. Mostrar o nome dum robo");
                System.out.println("5. Mostrar a equipa dum robo");
                System.out.println("6. Saber os dados duma prova");
                System.out.println("0. Sair");
                escolha = Utils.readLineFromConsole("Introduza opção: ");
                
                if (escolha.equals("1")){
                    EquipaUI ui = new EquipaUI(conn);
                    ui.getEquipa();
                }
                
                if (escolha.equals("2")){
                    PertenceUI ui = new PertenceUI(conn);
                    ui.getPertence();
                }
                
                
                if (escolha.equals("3")){
                    ParametroRoboUI ui = new ParametroRoboUI(conn);
                    ui.getParametro();
                }
                
                if (escolha.equals("4")){
                    RoboUI ui = new RoboUI(conn);
                    ui.getRobo();
                }
                if (escolha.equals("5")){
                    RoboEquipaUI ui= new RoboEquipaUI(conn);
                    ui.getRoboEquipa();
                }
                
                if (escolha.equals("6")){
                    ProvaRoboUI ui= new ProvaRoboUI(conn);
                    ui.getProvaRobo();
                }
                }while (!escolha.equals("0") );
                
                }
            
            if( opcao.equals("10"))
            { 
            MENUME menu =new MENUME();
            menu.run();
            }
        }  
        while (!opcao.equals("0") );
        
          
        
        }
        }

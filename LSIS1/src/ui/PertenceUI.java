/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.PertenceController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class PertenceUI {
     private final PertenceController controller;
    
    public PertenceUI(Connection connector){
        controller = new PertenceController(connector);
    }
    
    public void postPertence() throws Exception{
        
        int IDElemento=Utils.IntFromConsole("Introduza o número identificador do elemento: ");
        int IDEquipa= Utils.IntFromConsole("Introduza o número identificador da equipa: ");
        
        controller.post(IDEquipa, IDElemento);
    }
    
    public void deletePertence() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do elemento a eliminar da equipa ");
        
        controller.delete(id);
    }
    
    public void getPertence() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID da equipa que pretende mostar os elementos");
        
        controller.get(id);
    }
    
}

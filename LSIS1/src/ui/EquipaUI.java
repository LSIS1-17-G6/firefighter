/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.ElementoController;
import controller.EquipaController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class EquipaUI {
    private final EquipaController controller;
    
    public EquipaUI(Connection connector){
        controller = new EquipaController(connector);
    }
    
    public void postEquipa() throws Exception{
        
        int IDEquipa=Utils.IntFromConsole("Introduza o número identificador da equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Introduza o nome da equipa: ");
        
       controller.post(IDEquipa, nomeEquipa);
}
    public void editEquipa() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID da equipa que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDEquipa=Utils.IntFromConsole("Número identificador do equipa: ");
        String nomeEquipa= Utils.readLineFromConsole("Nome da equipa: ");
        
        controller.update(idProcura, IDEquipa, nomeEquipa);
        
    }
    
    public void deleteEquipa() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do equipa a eliminar: ");
        
        controller.delete(id);
    }
    
    public void getEquipa() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID da equipa que pretende saber o nome");
        
        controller.get(id);
    }
    
}
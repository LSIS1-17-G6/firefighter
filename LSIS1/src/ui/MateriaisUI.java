/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.MateriaisController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class MateriaisUI {
    private final MateriaisController controller;
    
    public MateriaisUI(Connection connector){
        controller = new MateriaisController(connector);
    }
    
    public void postMateriais() throws Exception{
        
        int IDMaterial=Utils.IntFromConsole("Introduza o número identificador do material: ");
        String Descricao= Utils.readLineFromConsole("Introduza a descricao do material: ");
        
       controller.post(IDMaterial,Descricao);
}
    public void editMateriais() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID do material que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDMaterial=Utils.IntFromConsole("Número identificador do material: ");
        String Descricao= Utils.readLineFromConsole("Descricao: ");
        
        controller.update(idProcura, IDMaterial, Descricao);
        
    }
    
    public void deleteMateriais() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID do material a eliminar: ");
        
        controller.delete(id);
    }
    
    
}

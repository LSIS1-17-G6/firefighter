/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import controller.ProvaRoboController;
import java.sql.Connection;
import utils.Utils;

/**
 *
 * @author Utilizador
 */
public class ProvaRoboUI {
    private final ProvaRoboController controller;
    
    public ProvaRoboUI(Connection connector){
        
    
        controller = new ProvaRoboController(connector);     
    }
    
    public void postProvaRobo() throws Exception{
        
        int IDProva=Utils.IntFromConsole("Introduza o numero identificador da Prova: ");
        int IDRobo=Utils.IntFromConsole("Introduza o numero identificador do robo");
        String Comportamento= Utils.readLineFromConsole("Introduza Comportamento do robo: ");
        
        controller.post(IDProva, IDRobo, Comportamento);
}
    
public void editProvaRobo() throws Exception{
        int idProcura= Utils.IntFromConsole("Introduza o ID da prova que pretende efetuar alterações: ");
        System.out.println("Introduza os novos dados");
        int IDProva=Utils.IntFromConsole("Numero identificador da Prova: ");
        int IDRobo=Utils.IntFromConsole("Numero identificador do Robo");
        String Comportamento= Utils.readLineFromConsole("Comportamento: ");
        
        controller.update(idProcura, IDProva, IDRobo,Comportamento);
}

 public void deleteProvaRobo() throws Exception{
        
        int id=Utils.IntFromConsole("Indique o ID da Prova a eliminar: ");
        
        controller.delete(id);
    }
 
 public void getProvaRobo() throws Exception{
     int id= Utils.IntFromConsole("Indique o identificador da prova que pretende saber os dados: ");
     
     controller.get(id);
 }
    
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

//import com.mysql.jdbc.Connection;



import java.sql.Connection;
import model.Elemento;
import utils.BDConnect;

public class Main {
    public static Connection conn;

    public static void main(String[] args) {
       
        try {
            conn = BDConnect.getConnection();
            
            
            MenuUI uiMenu = new MenuUI(conn);

            uiMenu.run();
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}

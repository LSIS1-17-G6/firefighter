/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.PreparedStatement;
import static controller.ElementoController.conn;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Elemento;
import model.Equipa;

/**
 *
 * @author Utilizador
 */
public class EquipaController {
    
     List<Equipa> equipas;
    static Connection conn;
    
    public EquipaController(Connection conexao){
        
        equipas = new ArrayList<>();
        conn = conexao;
    }
    
    public void post(int ID, String nome) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `equipa` (`IDEquipa`,`nomeEquipa`) VALUES ('"+ID+"','"+nome+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Equipa inserida");
}
}
    public void update(int idProcura, int ID, String nome) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `equipa` SET `IDEquipa`='"+ID+"',`nomeEquipa`='"+nome+"' WHERE `IDEquipa`='"+idProcura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa atualizado");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `equipa` WHERE `IDEquipa`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Equipa apagada");
        }
    }
    
     public void get(int idprocura) throws Exception{
      
	try {
            
        PreparedStatement select= (PreparedStatement) conn.prepareStatement("SELECT `nomeEquipa` FROM `equipa` WHERE `IDEquipa`= '"+idprocura+"' ");
        
	ResultSet result= select.executeQuery();
        
        
        while (result.next()){
            System.out.println("Nome da Equipa: "+result.getString("nomeEquipa"));
        
        }
            System.out.println("Terminado");
            
	} catch(Exception e) {
	System.out.println(e);}
}
    
}

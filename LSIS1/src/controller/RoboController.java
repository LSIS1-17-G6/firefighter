/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author Utilizador
 */
public class RoboController {
    
    static Connection conn;
    
    public RoboController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int IDRobo, String nomeRobo) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `robo` (`IDRobo`,`nomeRobo`) VALUES ('"+IDRobo+"','"+nomeRobo+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Robo inserido");
}
}
    
    public void update(int idprocura, int IDrobo, String nomerobo) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `robo` SET `IDRobo`='"+IDrobo+"',`nomeRobo`='"+nomerobo+"' WHERE `IDRobo`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Informacao de robo atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `robo` WHERE `IDRobo`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Registo de robo apagado");
        }
    }
    
    public void get(int idprocura) throws Exception{
      
	try {
            
        PreparedStatement select= (PreparedStatement) conn.prepareStatement("SELECT * FROM `robo` WHERE `IDRobo`= '"+idprocura+"' ");
        
	ResultSet result= select.executeQuery();
        
        
        while (result.next()){
            System.out.println("Código robo: "+result.getInt("IDRobo")+ " Nome do robo: "+ result.getString("nomeRobo"));
        
        }
            System.out.println("Terminado");
            
	} catch(Exception e) {
	System.out.println(e);}
}
    
}

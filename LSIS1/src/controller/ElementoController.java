/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Elemento;
/**
 *
 * @author Utilizador
 */
public class ElementoController {
    
    List<Elemento> elementos;
    static Connection conn;
    
    public ElementoController(Connection conexao){
        
        elementos = new ArrayList<>();
        conn = conexao;
    }
    
    public void post(int ID, String nome, int idade) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `elemento` (`IDElemento`,`nomeElemento`, `idade`) VALUES ('"+ID+"','"+nome+"', '"+idade+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Elemento inserido");
}
}
    
    public void update(int idprocura, int ID, String nome, int idade) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `elemento` SET `IDElemento`='"+ID+"',`nomeElemento`='"+nome+"',`idade`='"+idade+"' WHERE `IDElemento`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Elemento atualizado");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `elemento` WHERE `IDElemento`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Elemento apagado");
        }
    }
    
    
}
    

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author Utilizador
 */
public class ParametroRoboController {
    
    static Connection conn;
    
    public  ParametroRoboController(Connection conexao){
        conn = conexao;
    
    }
    
    public void post(int IDR, int IDM) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `parametrorobo` (`IDRobo`,`IDMaterial`) VALUES ('"+IDR+"','"+IDM+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Componente do robo inserido");
}
}
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `parametrorobo` WHERE `IDMaterial`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Componente do robo apagado ");
        }
    }
    
    public void get(int idprocura) throws Exception{
      
	try {
            
        PreparedStatement select= (PreparedStatement) conn.prepareStatement("SELECT p.IDMaterial, m.Descricao FROM parametrorobo p INNER JOIN materiais m ON p.IDMaterial= m.IDMaterial WHERE `IDRobo`= '"+idprocura+"' ");
        
	ResultSet result= select.executeQuery();
        
        
        while (result.next()){
            System.out.println("Código do Material: "+result.getInt("IDMaterial") + " Descricao: " + result.getString("Descricao"));
        
        }
            System.out.println("Todos os materiais");
            
	} catch(Exception e) {
	System.out.println(e);}
}
    
}

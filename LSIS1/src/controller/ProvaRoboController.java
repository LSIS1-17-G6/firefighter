/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.mysql.jdbc.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author Utilizador
 */
public class ProvaRoboController {
    
    static Connection conn;
    
    public ProvaRoboController(Connection conexao){
        
        conn = conexao;
    }
    
    public void post(int IDP, int IDR, String comportamento) throws Exception{
     
	try {
            
	PreparedStatement posted = (PreparedStatement) conn.prepareStatement("INSERT INTO `provarobo` (`IDProva`,`IDRobo`,`Comportamento`) VALUES ('"+IDP+"','"+IDR+"','"+comportamento+"')");
	posted.executeUpdate();
	} catch(Exception e) {
	System.out.println(e);}
	finally {
            System.out.println("Prova inserida");
}
}
    
    public void update(int idProcura, int IDP, int IDR, String comportamento) throws Exception{
        
        try {
            
           PreparedStatement update= (PreparedStatement) conn.prepareStatement("UPDATE `provarobo` SET `IDProva`='"+IDP+"',`IDRobo`='"+IDR+"',`Comportamento`='"+comportamento+"'  WHERE `IDProva`='"+idProcura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Prova atualizada");
        }
    }
    
    public void delete(int idprocura) throws Exception{ 
        try {

           PreparedStatement update= (PreparedStatement) conn.prepareStatement("DELETE FROM `provarobo` WHERE `IDProva`='"+idprocura+"'");
           
           update.executeUpdate();
        } catch (Exception e){
            System.out.println(e);}
        finally {
            System.out.println("Prova apagada");
        }
    }
    
    public void get(int idprocura) throws Exception{
      
	try {
            
        PreparedStatement select= (PreparedStatement) conn.prepareStatement("SELECT * FROM `provarobo` WHERE `IDProva`= '"+idprocura+"' ");
        
	ResultSet result= select.executeQuery();
        
        
        while (result.next()){
            System.out.println("Codigo da prova: "+result.getInt("IDProva")+ " Codigo do robo: "+result.getInt("IDRobo") + " Comportamento: "+ result.getString("Comportamento"));
        
        }
            System.out.println("Todos os dados da prova");
            
	} catch(Exception e) {
	System.out.println(e);}
}
}

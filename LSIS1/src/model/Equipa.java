/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class Equipa {
    private int m_IDEquipa;
    private String m_nomeEquipa;
    
    public Equipa() {
    }
    
    public Equipa(int m_IDEquipa,String m_nomeEquipa){
        this.m_IDEquipa=m_IDEquipa;
        this.m_nomeEquipa=m_nomeEquipa;
    }

    /**
     * @return the m_IDEquipa
     */
    public int getM_IDEquipa() {
        return m_IDEquipa;
    }

    /**
     * @param m_IDEquipa the m_IDEquipa to set
     */
    public void setM_IDEquipa(int m_IDEquipa) {
        this.m_IDEquipa = m_IDEquipa;
    }

    /**
     * @return the m_nomeEquipa
     */
    public String getM_nomeEquipa() {
        return m_nomeEquipa;
    }

    /**
     * @param m_nomeEquipa the m_nomeEquipa to set
     */
    public void setM_nomeEquipa(String m_nomeEquipa) {
        this.m_nomeEquipa = m_nomeEquipa;
    }


    @Override
    public String toString() {
        return "Equipa: " + "ID: " + m_IDEquipa + "nome: " + m_nomeEquipa +'}';
    }
    
    
}

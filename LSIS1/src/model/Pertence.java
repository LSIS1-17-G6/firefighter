/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class Pertence {
    private int m_IDElemento;
    private int m_IDEquipa;
    
    public Pertence() {
    }
    
    public Pertence(int m_IDElemento, int m_IDEquipa){
        this.m_IDElemento=m_IDElemento;
        this.m_IDEquipa=m_IDEquipa;
    }

    /**
     * @return the m_IDElemento
     */
    public int getM_IDElemento() {
        return m_IDElemento;
    }

    /**
     * @param m_IDElemento the m_IDElemento to set
     */
    public void setM_IDElemento(int m_IDElemento) {
        this.m_IDElemento = m_IDElemento;
    }

    /**
     * @return the m_IDEquipa
     */
    public int getM_IDEquipa() {
        return m_IDEquipa;
    }

    /**
     * @param m_IDEquipa the m_IDEquipa to set
     */
    public void setM_IDEquipa(int m_IDEquipa) {
        this.m_IDEquipa = m_IDEquipa;
    }
    
    @Override
    public String toString() {
        return "Pertence: " + "ID do elemento " + m_IDElemento + "ID da equipa " + m_IDEquipa + '}';
    } 
    
    
}

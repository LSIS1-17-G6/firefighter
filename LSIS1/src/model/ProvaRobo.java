/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class ProvaRobo {
    private int m_IDProva;
    private int m_IDRobo;
    private String m_Comportamento;
    
    public ProvaRobo() {
    }
    
    public ProvaRobo(int m_IDProva,int m_IDRobo, String m_Comportamento){
        this.m_IDProva= m_IDProva;
        this.m_IDRobo= m_IDRobo;
        this.m_Comportamento= m_Comportamento;
    }

    /**
     * @return the m_IDProva
     */
    public int getM_IDProva() {
        return m_IDProva;
    }

    /**
     * @param m_IDProva the m_IDProva to set
     */
    public void setM_IDProva(int m_IDProva) {
        this.m_IDProva = m_IDProva;
    }

    /**
     * @return the m_IDRobo
     */
    public int getM_IDRobo() {
        return m_IDRobo;
    }

    /**
     * @param m_IDRobo the m_IDRobo to set
     */
    public void setM_IDRobo(int m_IDRobo) {
        this.m_IDRobo = m_IDRobo;
    }

    /**
     * @return the m_Comportamento
     */
    public String getM_Comportamento() {
        return m_Comportamento;
    }

    /**
     * @param m_Comportamento the m_Comportamento to set
     */
    public void setM_Comportamento(String m_Comportamento) {
        this.m_Comportamento = m_Comportamento;
    }
    
    @Override
    public String toString() {
        return "Prova: " + "ID da Prova: " + m_IDProva + "Robo n. : " + m_IDRobo + "Comportamento: "+ m_Comportamento + '}';
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class Robo {
    private int m_IDRobo;
    private String m_nomeRobo;
    
    public Robo() {
    }
    
    public Robo(int m_IDRobo, String m_nomeRobo){
        this.m_IDRobo=m_IDRobo;
        this.m_nomeRobo=m_nomeRobo;
    }

    /**
     * @return the m_IDRobo
     */
    public int getM_IDRobo() {
        return m_IDRobo;
    }

    /**
     * @param m_IDRobo the m_IDRobo to set
     */
    public void setM_IDRobo(int m_IDRobo) {
        this.m_IDRobo = m_IDRobo;
    }

    /**
     * @return the m_nomeRobo
     */
    public String getM_nomeRobo() {
        return m_nomeRobo;
    }

    /**
     * @param m_nomeRobo the m_nomeRobo to set
     */
    public void setM_nomeRobo(String m_nomeRobo) {
        this.m_nomeRobo = m_nomeRobo;
    }
    
    @Override
    public String toString() {
        return "Robo: " + "ID: " + m_IDRobo + "nome: " + m_nomeRobo+ '}';
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class Elemento {
    private int m_IDElemento;
    private String m_nomeElemento;
    private int m_idade;
    
    public Elemento() {
    }
    
    public Elemento(int m_IDElemento, String m_nomeElemento, int m_idade){
        this.m_IDElemento=m_IDElemento;
        this.m_nomeElemento=m_nomeElemento;
        this.m_idade= m_idade;
    }

    /**
     * @return the m_IDElemento
     */
    public int getM_IDElemento() {
        return m_IDElemento;
    }

    /**
     * @param m_IDElemento the m_IDElemento to set
     */
    public void setM_IDElemento(int m_IDElemento) {
        this.m_IDElemento = m_IDElemento;
    }

    /**
     * @return the m_nomeElemento
     */
    public String getM_nomeElemento() {
        return m_nomeElemento;
    }

    /**
     * @param m_nomeElemento the m_nomeElemento to set
     */
    public void setM_nomeElemento(String m_nomeElemento) {
        this.m_nomeElemento = m_nomeElemento;
    }

    /**
     * @return the m_idade
     */
    public int getM_idade() {
        return m_idade;
    }

    /**
     * @param m_idade the m_idade to set
     */
    public void setM_idade(int m_idade) {
        this.m_idade = m_idade;
    }

    @Override
    public String toString() {
        return "Elemento: " + "ID: " + m_IDElemento + "nome: " + m_nomeElemento + " idade: " + m_idade + '}';
    } 
    
    }
    

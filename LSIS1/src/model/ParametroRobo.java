/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class ParametroRobo {
    private int m_IDRobo;
    private int m_IDMaterial;
    
    public ParametroRobo() {
    }
    
    public ParametroRobo(int m_IDRobo, int m_IDMaterial){
        this.m_IDRobo=m_IDRobo;
        this.m_IDMaterial=m_IDMaterial;
    }

    /**
     * @return the m_IDRobo
     */
    public int getM_IDRobo() {
        return m_IDRobo;
    }

    /**
     * @param m_IDRobo the m_IDRobo to set
     */
    public void setM_IDRobo(int m_IDRobo) {
        this.m_IDRobo = m_IDRobo;
    }

    /**
     * @return the m_IDMaterial
     */
    public int getM_IDMaterial() {
        return m_IDMaterial;
    }

    /**
     * @param m_IDMaterial the m_IDMaterial to set
     */
    public void setM_IDMaterial(int m_IDMaterial) {
        this.m_IDMaterial = m_IDMaterial;
    }
    
    @Override
    public String toString() {
        return "Parametros do Robo: " + "ID do Robo " + m_IDRobo + "ID do Material " + m_IDMaterial + '}';
    } 
    
    
}

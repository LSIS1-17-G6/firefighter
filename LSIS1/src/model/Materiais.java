/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class Materiais {
    private int m_IDMaterial;
    private String m_Descricao;
    
    public Materiais() {
    }
    
    public Materiais(int m_IDMaterial, String m_Descricao){
        this.m_IDMaterial= m_IDMaterial;
        this.m_Descricao= m_Descricao;
    }

    /**
     * @return the m_IDMaterial
     */
    public int getM_IDMaterial() {
        return m_IDMaterial;
    }

    /**
     * @param m_IDMaterial the m_IDMaterial to set
     */
    public void setM_IDMaterial(int m_IDMaterial) {
        this.m_IDMaterial = m_IDMaterial;
    }

    /**
     * @return the m_Descricao
     */
    public String getM_Descricao() {
        return m_Descricao;
    }

    /**
     * @param m_Descricao the m_Descricao to set
     */
    public void setM_Descricao(String m_Descricao) {
        this.m_Descricao = m_Descricao;
    }
    
    @Override
    public String toString() {
        return "Material: " + "ID: " + m_IDMaterial + "Descricao: " + m_Descricao +'}';
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Utilizador
 */
public class RoboEquipa {
    private int m_IDRobo;
    private int m_IDEquipa;
    
    public RoboEquipa() {
    }
    
    public RoboEquipa(int m_IDRobo, int m_IDEquipa){
        this.m_IDRobo=m_IDRobo;
        this.m_IDEquipa=m_IDEquipa;
    }

    /**
     * @return the m_IDRobo
     */
    public int getM_IDRobo() {
        return m_IDRobo;
    }

    /**
     * @param m_IDRobo the m_IDRobo to set
     */
    public void setM_IDRobo(int m_IDRobo) {
        this.m_IDRobo = m_IDRobo;
    }

    /**
     * @return the m_IDEquipa
     */
    public int getM_IDEquipa() {
        return m_IDEquipa;
    }

    /**
     * @param m_IDEquipa the m_IDEquipa to set
     */
    public void setM_IDEquipa(int m_IDEquipa) {
        this.m_IDEquipa = m_IDEquipa;
    }
    
    @Override
    public String toString() {
        return "Robo da Equipa: " + "ID do robo " + m_IDRobo + "ID da equipa " + m_IDEquipa + '}';
    }
}

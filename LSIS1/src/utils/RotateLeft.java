package utils;

import java.awt.EventQueue;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import static java.awt.SystemColor.text;
import java.awt.geom.AffineTransform;
import java.awt.geom.CubicCurve2D;
import static java.lang.Thread.sleep;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javafx.scene.text.*;


public class RotateLeft  {

    JFrame j = new JFrame();
    DrawPanel d = new DrawPanel();
    
    public RotateLeft() {

        initUI();
        
    }
    public void close(){
        j.setVisible(false);
        j.dispose();
    }

    private void initUI() {
        
        j.setTitle("Maquina de Estados");
        j.setSize(1000, 750);
        j.setLocationRelativeTo(null);
        j.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        
        j.add(d);
        j.setVisible(true);
        d.setOpaque(false);
    }
     
    class DrawPanel extends JPanel {

    private void doDrawing(Graphics g) throws InterruptedException {
        

        Graphics2D g2d = (Graphics2D) g;

        g2d.setColor(Color.blue);

        //for (int i = 0; i <= 1000; i++) {

            Dimension size = getSize();
            Insets insets = getInsets();

            int w = size.width - insets.left - insets.right;
            int h = size.height - insets.top - insets.bottom;

                        
        // circles 
               
            g2d.drawOval(140, 100, 80, 80); 
            g2d.drawOval(360, 190, 80, 80);
            g2d.drawOval(560, 100, 80, 80);
            g2d.drawOval(140, 190, 80, 80); // esquerda centro
            g2d.drawOval(560, 190, 80, 80);
            g2d.drawOval(360, 40, 80, 80);
            g2d.drawOval(360, 360, 80, 80); // baixo centro
            g2d.drawOval(600, 360, 80, 80);
            g2d.drawOval(140, 360, 80, 80);
            g2d.drawOval(360, 480, 80, 80);
            g2d.drawOval(360, 630, 80, 80);
            g2d.drawOval(600, 630, 80, 80);
       // lines   

           g2d.drawLine(400,270, 400, 360);
           g2d.drawLine(400,120, 400, 190);
           g2d.drawLine(428,202, 560, 140);
           g2d.drawLine(439,218, 572, 168);
           g2d.drawLine(372,202, 220, 140);
           g2d.drawLine(440,230, 560, 230);
           g2d.drawLine(572,258, 530, 350);
           g2d.drawLine(440,400, 530, 350);
           g2d.drawLine(440,400, 600, 400);
           g2d.drawLine(428,258, 460, 310);
           g2d.drawLine(460,310, 428, 372);
           g2d.drawLine(372,258, 320, 280);
           g2d.drawLine(320,280, 320, 340);
           g2d.drawLine(320,340, 372, 372);
           g2d.drawLine(220,230, 360, 230);
           g2d.drawLine(366,250, 230, 280);
           g2d.drawLine(230,280, 230, 340);
           g2d.drawLine(230,340, 360, 400);
           g2d.drawLine(360,400, 210, 360);
           g2d.drawLine(210,360, 180, 270);
           g2d.drawLine(360,400, 220, 400);
           g2d.drawLine(208,428, 360, 520);
           g2d.drawLine(400,560, 400, 630);
           g2d.drawLine(440,520, 612, 428);
           g2d.drawLine(440,670, 600, 670);
           g2d.drawLine(384,192, 384, 118);
           
           
       // arrows
       //F>20
      g2d.drawLine(400, 360, 398, 354);
      g2d.drawLine(400, 360, 402, 354);
       // Start
      g2d.drawLine(400, 190, 398, 184);
      g2d.drawLine(400, 190, 402, 184);   
       // L<10
      g2d.drawLine(560, 140, 554, 140);
      g2d.drawLine(560, 140, 556, 145);
      // F<20^R>10^L>10
      g2d.drawLine(572, 168, 566, 168);
      g2d.drawLine(572, 168, 568, 173);    
      // F>20^R>L
      g2d.drawLine(560, 230, 554, 232);
      g2d.drawLine(560, 230, 554, 228);
      // 
      g2d.drawLine(572, 258, 566, 262);
      g2d.drawLine(572, 258, 572, 266);
          g2d.drawLine(440, 400, 446, 392);
          g2d.drawLine(440, 400, 448, 398);
      // count2<3
      g2d.drawLine(600, 400, 594, 397);
      g2d.drawLine(600, 400, 594, 403);    
      // F>60^Timer>30
      g2d.drawLine(428, 372, 428, 366);
      g2d.drawLine(428, 372, 432, 370);
      // 80>F>110^10>R>20^57>L>76
      g2d.drawLine(372, 372, 370, 366);
      g2d.drawLine(372, 372, 364, 372);
      // 80>F>110^10>L>20^57>R>76
      g2d.drawLine(360, 400, 354, 396);
      g2d.drawLine(360, 400, 350, 400 );
      g2d.drawLine(360, 400, 352, 394 );
      // Quarto2
      g2d.drawLine(220, 400, 226, 396 );
      g2d.drawLine(220, 400, 226, 404 );
      //RotateLeft90
      g2d.drawLine(180, 270, 177, 276 );
      g2d.drawLine(180, 270, 185, 275 );
      //F>20^R<L
      g2d.drawLine(220, 230, 224, 226 );
      g2d.drawLine(220, 230, 224, 234 );
      //R<10
      g2d.drawLine(220, 140, 226, 138 );
      g2d.drawLine(220, 140, 224, 146 );
      //Flame Sensor
      g2d.drawLine(360, 520, 354, 512 );
      g2d.drawLine(360, 520, 350, 520 );
      //flame>980
      g2d.drawLine(400, 630, 396, 624 );
      g2d.drawLine(400, 630, 404, 624 );
      //flame>80
      g2d.drawLine(440, 520, 446, 512 );
      g2d.drawLine(440, 520, 446, 522 );
      //
      g2d.drawLine(600, 670, 596, 666 );
      g2d.drawLine(600, 670, 596, 674 );
      // Stop
      g2d.drawLine(384, 118, 380, 122);
      g2d.drawLine(384, 118, 388, 122);
       
       
       
       
        // text circles
        g2d.drawString("IDLE", 385, 230);
        g2d.drawString("IDLE", 385, 80);
        g2d.drawString("Rotate", 585, 140);
        g2d.drawString("Right", 585, 155);
        g2d.drawString("Rotate", 585, 225);
        g2d.drawString("Right90", 585, 240); 
        g2d.drawString("Quarto", 620, 402);
        g2d.drawString("Forward", 379, 402); 
        g2d.drawString("Quarto2", 155, 402);
        g2d.drawString("Rotate", 165, 225);
        g2d.drawString("Left90", 165, 240);
        g2d.drawString("Rotate", 165, 140);
        g2d.drawString("Left", 165, 155);
        g2d.drawString("Flame", 380, 520);
        g2d.drawString("Sensor", 380, 535);
        g2d.drawString("Propeller", 375, 675);
        g2d.drawString("IDLE", 625, 672);
        
        
        
        // text arrows
        g2d.drawString("Start", 410, 155);
        g2d.drawString("F>20^R<L", 240, 225);
        g2d.drawString("F>20^R>L", 470, 225);
        g2d.drawString("F>20", 405, 325);
        g2d.drawString("Count2<3", 505, 395);
        g2d.drawString("F>60^", 460, 325);
        g2d.drawString("Timer>30", 460, 340);
        g2d.drawString("80>F>110", 335, 288);
        g2d.drawString("^", 335, 303);
        g2d.drawString("10>R>20", 335, 318);
        g2d.drawString("^", 335, 333);
        g2d.drawString("57>L>76", 335, 348);
        g2d.drawString("80>F>110", 248, 288);
        g2d.drawString("^", 248, 303);
        g2d.drawString("10>L>20", 248, 318);
        g2d.drawString("^", 248, 333);
        g2d.drawString("57>R>76", 248, 348);
        g2d.drawString("L<10", 480, 160);
        g2d.drawString("F<20^R>10^L>10", 480, 190); 
        g2d.drawString("R<10", 300, 170);
        g2d.drawString("flame>80", 260, 460);
        g2d.drawString("flame>980", 405, 590);
        g2d.drawString("flame>80", 480, 460);
        g2d.drawString("flame<80", 490, 665);
        g2d.drawString("Stop", 350, 150);

 // tansformation   

    g2d.setColor(Color.GREEN);

        g2d.fillOval(140, 100, 80, 80);

        g2d.setColor(Color.BLUE);
        
        g2d.drawString("Rotate", 165, 140);
        g2d.drawString("Left", 165, 155);         
    
  }

    
    @Override
    public void paintComponent(Graphics g) {

            paintComponents(g);
        try {
            doDrawing(g);
        } catch (InterruptedException ex) {
            Logger.getLogger(MEstados.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    }
}
    
       

   



